#Automake demo 

hello: hello.o
	gcc -o hello hello.o 

hello.o:hello.c
	gcc -c hello.c -o hello.o -g -Wall -pedantic --std=c99

clean:
	rm -rf hello
	rm -rf hello.o
